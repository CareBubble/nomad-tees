<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <title>Nomad Tees - We make Tees for Nomads</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,900%7COpen+Sans:300,300i,400,400i,600,600i,700,700i&amp;subset=cyrillic" rel="stylesheet">

    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/ionicons.min.css">

    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/jquery.formstyler.css">
    <link rel="stylesheet" href="/css/flexslider.css">
    <link rel="stylesheet" href="/css/jquery.fancybox.css">
    <link rel="stylesheet" href="/css/ion.rangeSlider.css">
    <link rel="stylesheet" href="/css/jquery.mThumbnailScroller.css">
    <link rel="stylesheet" href="/css/chosen.css">

    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/elements.css">
    <link rel="stylesheet" href="/css/media.css">
    <link rel="stylesheet" href="/css/elements-media.css">
    <style type="text/css">
        .glow {
                  -webkit-text-stroke: 2px white; /* width and color */

            }
    </style>

</head>
<body style="margin-top: 0px!important;"><div id="page" class="site header-sticky">


    <div class="site-header">

        <p class="h-logo">
            <a href="index.html"><img src="img/logo.png" alt="MultiShop"></a>
        </p><!--
    No Space
    --><div class="h-shop">

        <form method="get" action="#" class="h-search" id="h-search">
            <input type="text" placeholder="Search...">
            <button type="submit"><i class="ion-search"></i></button>
        </form>

        <ul class="h-shop-links">
            <li class="h-search-btn" id="h-search-btn"><i class="ion-search"></i></li>
            <li class="h-shop-icon h-wishlist">
                <a title="Wishlist" href="wishlist.html">
                    <i class="ion-heart"></i>
                    <span>5</span>
                </a>
            </li>
            <li class="h-shop-icon h-compare">
                <a title="Compare List" href="compare.html">
                    <i class="ion-arrow-swap"></i>
                    <span>2</span>
                </a>
            </li>
            <li class="h-shop-icon h-profile">
                <a href="auth.html" title="My Account">
                    <i class="ion-android-person"></i>
                </a>
                <ul class="h-profile-links">
                    <li><a href="auth.html">Login / Registration</a></li>
                    <li><a href="cart.html">Cart</a></li>
                    <li><a href="compare.html">Compare</a></li>
                    <li><a href="wishlist.html">Wishlist</a></li>
                </ul>
            </li>

            <li class="h-cart">
                <a class="cart-contents" href="cart.html">
                    <p class="h-cart-icon">
                        <i class="ion-android-cart"></i>
                        <span>3</span>
                    </p>
                    <p class="h-cart-total">$510.00</p>
                </a>
                <div class="widget_shopping_cart">
                    <div class="widget_shopping_cart_content">
                        <ul class="cart_list">
                            <li>
                                <a href="#" class="remove">&times;</a>
                                <a href="#">
                                    <img src="http://placehold.it/100x67" alt="">
                                    Pneumatic Coil Hose
                                </a>
                                <span class="quantity">1 &times; $180.00</span>
                            </li>
                            <li>
                                <a href="#" class="remove">&times;</a>
                                <a href="#">
                                    <img src="http://placehold.it/100x89" alt="">
                                    Drill Tool Kit
                                </a>
                                <span class="quantity">1 &times; $115.00</span>
                            </li>
                            <li>
                                <a href="#" class="remove">&times;</a>
                                <a href="#">
                                    <img src="http://placehold.it/100x89" alt="">
                                    Searchlight Portable
                                </a>
                                <span class="quantity">1 &times; $150.00</span>
                            </li>
                        </ul>
                        <p class="total"><b>Subtotal:</b> $510.00</p>
                        <p class="buttons">
                            <a href="cart.html" class="button">View cart</a>
                            <a href="checkout.html" class="button">Checkout</a>
                        </p>
                    </div>
                </div>
            </li>

            <li class="h-menu-btn" id="h-menu-btn">
                <i class="ion-navicon"></i> Menu
            </li>
        </ul>
    </div><!--
    No Space
    --><div class="mainmenu">

        <nav id="h-menu" class="h-menu">
            <ul>
                <li class="menu-item-has-children active">
                    <a href="index.html">Home</a>
                    <ul class="sub-menu">
                        <li>
                            <a href="index.html">Home 1</a>
                        </li>
                        <li class="active">
                            <a href="index-2.html">Home 2</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children">
                    <a href="catalog-gallery.html">Shop</a>
                    <ul class="sub-menu">
                        <li>
                            <a href="catalog-gallery.html">Catalog - Gallery</a>
                        </li>
                        <li>
                            <a href="catalog-list.html">Catalog - List</a>
                        </li>
                        <li>
                            <a href="catalog-gallery-full.html">Catalog - No Sidebar</a>
                        </li>
                        <li>
                            <a href="product.html">Product Page</a>
                        </li>
                        <li class="menu-item-has-children">
                            <a href="cart.html">Shop Pages</a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="cart.html">Cart</a>
                                </li>
                                <li>
                                    <a href="wishlist.html">Wishlist</a>
                                </li>
                                <li>
                                    <a href="compare.html">Compare</a>
                                </li>
                                <li>
                                    <a href="auth.html">Login</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="elements.html">Elements</a>
                </li>
                <li class="menu-item-has-children">
                    <a href="blog.html">Blog</a>
                    <ul class="sub-menu">
                        <li>
                            <a href="blog.html">Blog Posts</a>
                        </li>
                        <li>
                            <a href="post.html">Standard Post</a>
                        </li>
                        <li>
                            <a href="post-slider.html">Slider Post</a>
                        </li>
                    </ul>
                </li>
                <li class="menu-item-has-children">
                    <a href="about.html">Pages</a>
                    <ul class="sub-menu">
                        <li>
                            <a href="about.html">About Us</a>
                        </li>
                        <li>
                            <a href="contacts.html">Contacts</a>
                        </li>
                        <li>
                            <a href="gallery.html">Gallery</a>
                        </li>
                        <li>
                            <a href="404.html">Error 404</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>

    </div><!--
    No Space
--></div>


    <div id="content" class="site-content"><div id="primary" class="content-area width-full">
        <main id="main" class="site-main">


                <div class="heroblock2" style="margin-top: -167px; padding-top: 0px;">
                    <div class="heroblock2-img" style="width: 100%!important; ">
                        <h3 class="heroblock2-ttl glow" style="position: relative; top:500px;">We make Tees for Nomads</h3>
                        <img src="/img/hero.png" alt="" style="width: 100%!important; height: 100vh;">
                        <div class="heroblock2-caption">
                            <div class="heroblock2-price">$14,99</div>
                            <div class="heroblock2-price-desc">Incl. Taxes</div>
                        </div>
                    </div>


                <div class="container-fluid page-styling row-wrap-full align-center front-title-block2">
                    <h2>Read our story</h2>
                    <p style="max-width: 60%; margin: auto; margin-top: 20px; margin-bottom: 20px;">We're Nomad couple and we've been traveling full time together since the start of the lockdown, but James Vincero (AKA Remote Sensei) has been a Digital Nomad, since 2010.</p>
                    <a class="btn-multishop btn btn-lrg" href="catalog-gallery.html">Take me to the Tees!</a>
                    <a class="btn-multishop" href="catalog-gallery.html">Read More</a>
                </div>


                <div class="mb60 page-styling row-wrap-full front-image-half">
                    <section class="image-half image-half-left">
                        <div class="image-half-img" style="background-image: url('/img/home-surf.png');">
                            <img src="http://placehold.it/1006x498" alt="">
                        </div>
                        <div class="cont image-half-cont">
                            <div class="image-half-inner">
                                <h2>Get exclusive discounts</h2>
                                <form action="#" method="post" class="mb55 wpcf7 wpcf7-form">
                                    <p class="form-submit">
                        <span class="wpcf7-form-control-wrap">
                            <input type="email" placeholder="E-mail address">
                        </span>
                                        <input type="submit" value="Get Discounts!">
                                    </p>
                                </form>
                            </div>
                        </div>
                    </section>
                </div>

                <div class="container mb60 page-styling row-wrap-container row-wrap-nottl">
                    <p class="maincont-subttl">SPECIAL DEALS</p>
                    <h2 class="mb35 heading-multishop">Our Tees</h2>
                    <div class="row prod-items prod-items-3">
                        <article class="cf-sm-6 cf-md-4 cf-lg-4 col-xs-6 col-sm-6 col-md-4 col-lg-4 sectgl-item">
                            <div class="sectgl prod-i">
                                <div class="prod-i-top">
                                    <a class="prod-i-img" href="product.html">
                                        <img src="/img/nomad.jpeg" alt="">
                                    </a>
                                    <div class="prod-i-actions">
                                        <div class="prod-i-actions-in">
                                            <div class="prod-li-favorites">
                                                <a href="wishlist.html" class="hover-label add_to_wishlist"><i class="icon ion-heart"></i><span>Add to Wishlist</span></a>
                                            </div>
                                            <p class="prod-quickview">
                                                <a href="#" class="hover-label quick-view"><i class="icon ion-plus"></i><span>Quick View</span></a>
                                            </p>
                                            <p class="prod-i-cart">
                                                <a href="#" class="hover-label prod-addbtn"><i class="icon ion-android-cart"></i><span>Add to Cart</span></a>
                                            </p>
                                            <p class="prod-li-compare">
                                                <a href="compare.html" class="hover-label prod-li-compare-btn"><span>Compare</span><i class="icon ion-arrow-swap"></i></a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="prod-i-bot">
                                    <div class="prod-i-info">
                                        <p class="prod-i-price">$120.00</p>
                                        <p class="prod-i-categ"><a href="catalog-gallery.html">T-shirts</a></p>
                                    </div>
                                    <h3 class="prod-i-ttl"><a href="product.html">Digital Nomad for life</a></h3>
                                </div>
                            </div>
                        </article>                    
                        <article class="cf-sm-6 cf-md-4 cf-lg-4 col-xs-6 col-sm-6 col-md-4 col-lg-4 sectgl-item">
                        <div class="sectgl prod-i">
                            <div class="prod-i-top">
                                <a class="prod-i-img" href="product.html">
                                    <img src="/img/pesos.jpeg" alt="">
                                </a>
                                <div class="prod-i-actions">
                                    <div class="prod-i-actions-in">
                                        <div class="prod-li-favorites">
                                            <a href="wishlist.html" class="hover-label add_to_wishlist"><i class="icon ion-heart"></i><span>Add to Wishlist</span></a>
                                        </div>
                                        <p class="prod-quickview">
                                            <a href="#" class="hover-label quick-view"><i class="icon ion-plus"></i><span>Quick View</span></a>
                                        </p>
                                        <p class="prod-i-cart">
                                            <a href="#" class="hover-label prod-addbtn"><i class="icon ion-android-cart"></i><span>Add to Cart</span></a>
                                        </p>
                                        <p class="prod-li-compare">
                                            <a href="compare.html" class="hover-label prod-li-compare-btn"><span>Compare</span><i class="icon ion-arrow-swap"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="prod-i-bot">
                                <div class="prod-i-info">
                                    <p class="prod-i-price"><del>$15.00</del> $20.00</p>
                                    <p class="prod-i-categ"><a href="catalog-gallery.html">T-Shirts</a></p>
                                </div>
                                <h3 class="prod-i-ttl"><a href="product.html">Spend in Pesos</a></h3>
                            </div>
                        </div>
                    </article>                    
                    <article class="cf-sm-6 cf-md-4 cf-lg-4 col-xs-6 col-sm-6 col-md-4 col-lg-4 sectgl-item">
                        <div class="sectgl prod-i">
                            <div class="prod-i-top">
                                <a class="prod-i-img" href="product.html">
                                    <img src="/img/rupiah.jpeg" alt="">
                                </a>
                                <div class="prod-i-actions">
                                    <div class="prod-i-actions-in">
                                        <div class="prod-li-favorites">
                                            <a href="wishlist.html" class="hover-label add_to_wishlist"><i class="icon ion-heart"></i><span>Add to Wishlist</span></a>
                                        </div>
                                        <p class="prod-quickview">
                                            <a href="#" class="hover-label quick-view"><i class="icon ion-plus"></i><span>Quick View</span></a>
                                        </p>
                                        <p class="prod-i-cart">
                                            <a href="#" class="hover-label prod-addbtn"><i class="icon ion-android-cart"></i><span>Add to Cart</span></a>
                                        </p>
                                        <p class="prod-li-compare">
                                            <a href="compare.html" class="hover-label prod-li-compare-btn"><span>Compare</span><i class="icon ion-arrow-swap"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="prod-i-bot">
                                <div class="prod-i-info">
                                    <p class="prod-i-price">$18.00</p>
                                    <p class="prod-i-categ"><a href="catalog-gallery.html">T-Shirts</a></p>
                                </div>
                                <h3 class="prod-i-ttl"><a href="product.html">Spend in Rupiah</a></h3>
                            </div>
                        </div>
                    </article>                    <article class="cf-sm-6 cf-md-4 cf-lg-4 col-xs-6 col-sm-6 col-md-4 col-lg-4 sectgl-item">
                        <div class="sectgl prod-i">
                            <div class="prod-i-top">
                                <a class="prod-i-img" href="product.html">
                                    <img src="/img/backpack-ceo.jpeg" alt="">
                                </a>
                                <div class="prod-i-actions">
                                    <div class="prod-i-actions-in">
                                        <div class="prod-li-favorites">
                                            <a href="wishlist.html" class="hover-label add_to_wishlist"><i class="icon ion-heart"></i><span>Add to Wishlist</span></a>
                                        </div>
                                        <p class="prod-quickview">
                                            <a href="#" class="hover-label quick-view"><i class="icon ion-plus"></i><span>Quick View</span></a>
                                        </p>
                                        <p class="prod-i-cart">
                                            <a href="#" class="hover-label prod-addbtn"><i class="icon ion-android-cart"></i><span>Add to Cart</span></a>
                                        </p>
                                        <p class="prod-li-compare">
                                            <a href="compare.html" class="hover-label prod-li-compare-btn"><span>Compare</span><i class="icon ion-arrow-swap"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="prod-i-bot">
                                <div class="prod-i-info">
                                    <p class="prod-i-price">$45.00</p>
                                    <p class="prod-i-categ"><a href="catalog-gallery.html">T-Shirts</a></p>
                                </div>
                                <h3 class="prod-i-ttl"><a href="product.html">Backpack CEO</a></h3>
                            </div>
                        </div>
                    </article>                    
                    <article class="cf-sm-6 cf-md-4 cf-lg-4 col-xs-6 col-sm-6 col-md-4 col-lg-4 sectgl-item">
                        <div class="sectgl prod-i">
                            <div class="prod-i-top">
                                <a class="prod-i-img" href="product.html">
                                    <img src="/img/pesos-cap.jpeg" alt="">
                                </a>
                                <div class="prod-i-actions">
                                    <div class="prod-i-actions-in">
                                        <div class="prod-li-favorites">
                                            <a href="wishlist.html" class="hover-label add_to_wishlist"><i class="icon ion-heart"></i><span>Add to Wishlist</span></a>
                                        </div>
                                        <p class="prod-quickview">
                                            <a href="#" class="hover-label quick-view"><i class="icon ion-plus"></i><span>Quick View</span></a>
                                        </p>
                                        <p class="prod-i-cart">
                                            <a href="#" class="hover-label prod-addbtn"><i class="icon ion-android-cart"></i><span>Add to Cart</span></a>
                                        </p>
                                        <p class="prod-li-compare">
                                            <a href="compare.html" class="hover-label prod-li-compare-btn"><span>Compare</span><i class="icon ion-arrow-swap"></i></a>
                                        </p>
                                    </div>
                                </div>
                                <p class="prod-i-badge"><span>Sale</span></p>
                            </div>
                            <div class="prod-i-bot">
                                <div class="prod-i-info">
                                    <p class="prod-i-price">$70.00</p>
                                    <p class="prod-i-categ"><a href="catalog-gallery.html">Baseball Caps</a></p>
                                </div>
                                <h3 class="prod-i-ttl"><a href="product.html">Spend In Pesos Cap</a></h3>
                            </div>
                        </div>
                    </article>                    
                    <article class="cf-sm-6 cf-md-4 cf-lg-4 col-xs-6 col-sm-6 col-md-4 col-lg-4 sectgl-item">
                        <div class="sectgl prod-i">
                            <div class="prod-i-top">
                                <a class="prod-i-img" href="product.html">
                                    <img src="/img/nomad.jpeg" alt="">
                                </a>
                                <div class="prod-i-actions">
                                    <div class="prod-i-actions-in">
                                        <div class="prod-li-favorites">
                                            <a href="wishlist.html" class="hover-label add_to_wishlist"><i class="icon ion-heart"></i><span>Add to Wishlist</span></a>
                                        </div>
                                        <p class="prod-quickview">
                                            <a href="#" class="hover-label quick-view"><i class="icon ion-plus"></i><span>Quick View</span></a>
                                        </p>
                                        <p class="prod-i-cart">
                                            <a href="#" class="hover-label prod-addbtn"><i class="icon ion-android-cart"></i><span>Add to Cart</span></a>
                                        </p>
                                        <p class="prod-li-compare">
                                            <a href="compare.html" class="hover-label prod-li-compare-btn"><span>Compare</span><i class="icon ion-arrow-swap"></i></a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="prod-i-bot">
                                <div class="prod-i-info">
                                    <p class="prod-i-price"><del>$12.00</del> $22.00</p>
                                    <p class="prod-i-categ"><a href="catalog-gallery.html">Baseball caps</a></p>
                                </div>
                                <h3 class="prod-i-ttl"><a href="product.html">Spend in Rupiah cap</a></h3>
                            </div>
                        </div>
                    </article>          
                </div>
                    <p class="special-more">
                        <a class="special-more-btn" href="#">Show More Products</a>
                    </p>
                </div>
                <!--
                <div class="container-fluid page-styling row-wrap-full front-icons2">
                    <div class="row">
                        <div class="cf-md-6 cf-lg-3 col-sm-6 col-md-6 col-lg-3">
                            <div class="iconbox-item iconbox-i-2">
                                <div class="iconbox-i-top">
                                    <p class="iconbox-i-img">
                                        <i class="fa fa-camera"></i>
                                    </p>
                                    <h3><a href="#">Aliquam erat volutpat</a></h3>
                                </div>
                                <p>Vivamus non viverra est. Suspendisse vitae tellus et felis ullamcorper dapibus vitae sed dui. Donec fringilla sollicitudin justo, in aliquet urna gravida vitae.</p>
                            </div>
                        </div>
                        <div class="cf-md-6 cf-lg-3 col-sm-6 col-md-6 col-lg-3">
                            <div class="iconbox-item iconbox-i-2">
                                <div class="iconbox-i-top">
                                    <p class="iconbox-i-img">
                                        <i class="fa fa-star"></i>
                                    </p>
                                    <h3><a href="#">Quisque non commodo augue</a></h3>
                                </div>
                                <p>Vivamus non viverra est. Suspendisse vitae tellus et felis ullamcorper dapibus vitae sed dui. Donec fringilla sollicitudin justo, in aliquet urna gravida vitae.</p>
                            </div>
                        </div>
                        <div class="cf-md-6 cf-lg-3 col-sm-6 col-md-6 col-lg-3">
                            <div class="iconbox-item iconbox-i-2">
                                <div class="iconbox-i-top">
                                    <p class="iconbox-i-img">
                                        <i class="fa fa-film"></i>
                                    </p>
                                    <h3><a href="#"> Aenean eu mauris urna</a></h3>
                                </div>
                                <p>Vivamus non viverra est. Suspendisse vitae tellus et felis ullamcorper dapibus vitae sed dui. Donec fringilla sollicitudin justo, in aliquet urna gravida vitae.</p>
                            </div>
                        </div>
                        <div class="cf-md-6 cf-lg-3 col-sm-6 col-md-6 col-lg-3">
                            <div class="iconbox-item iconbox-i-2">
                                <div class="iconbox-i-top">
                                    <p class="iconbox-i-img">
                                        <i class="fa fa-heart"></i>
                                    </p>
                                    <h3><a href="#">Maecenas interdum nisl non</a></h3>
                                </div>
                                <p>Vivamus non viverra est. Suspendisse vitae tellus et felis ullamcorper dapibus vitae sed dui. Donec fringilla sollicitudin justo, in aliquet urna gravida vitae.</p>
                            </div>
                        </div>
                    </div>
                </div>
                -->

                <div class="container-fluid page-styling row-wrap-full align-center front-team" style="margin-bottom: 0px;">
                    <h2>We Make Beautiful Things</h2>
                    <p class="front-team-subttl">We are also continuing to make new investments in our staff through training</p>
                    <div class="container">
                        <div class="row">
                            <div class="cf-md-6 cf-lg-3 col-sm-6 col-lg-3">
                                <div class="team-i">
                                    <p class="team-i-img">
                                        <img src="http://placehold.it/200x200" alt="">
                                    </p>
                                    <h3>Jim Cook</h3>
                                    <p class="team-i-position">Warehouse Assistant</p>
                                </div>
                            </div>
                            <div class="cf-md-6 cf-lg-3 col-sm-6 col-lg-3">
                                <div class="team-i">
                                    <p class="team-i-img">
                                        <img src="http://placehold.it/200x200" alt="">
                                    </p>
                                    <h3>Brent Kuhn</h3>
                                    <p class="team-i-position">Electrical worker</p>
                                </div>
                            </div>
                            <div class="cf-md-6 cf-lg-3 col-sm-6 col-lg-3">
                                <div class="team-i">
                                    <p class="team-i-img">
                                        <img src="http://placehold.it/200x200" alt="">
                                    </p>
                                    <h3>Laurie Bell</h3>
                                    <p class="team-i-position">Logistic Specialist</p>
                                </div>
                            </div>
                            <div class="cf-md-6 cf-lg-3 col-sm-6 col-lg-3">
                                <div class="team-i">
                                    <p class="team-i-img">
                                        <img src="http://placehold.it/200x200" alt="">
                                    </p>
                                    <h3>Jamie Lucas</h3>
                                    <p class="team-i-position">Welder</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .maincont.page-styling.page-full -->
        </main><!-- #main -->
    </div><!-- #primary -->    </div><!-- #content -->

    <div class="container-fluid blog-sb-widgets page-styling site-footer">
        <div class="row">
            <div class="col-sm-12 col-md-4 widget align-center-tablet f-logo-wrap">
                <a href="index.html" class="f-logo">
                    <img src="img/logo.png" alt="">
                </a>
                <p>Modern eCommerce HTML Template</p>
                <button class="btn callback">Contact Us</button>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-2 align-center-mobile widget">
                <h3 class="widgettitle">Company</h3>
                <ul class="menu">
                    <li>
                        <a href="index.html">Front Page</a>
                    </li>
                    <li>
                        <a href="about.html">About Us</a>
                    </li>
                    <li>
                        <a href="contacts.html">Contacts</a>
                    </li>
                    <li>
                        <a href="index.html">Gallery</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-2 align-center-mobile widget">
                <h3 class="widgettitle">Shop</h3>
                <ul class="menu">
                    <li>
                        <a href="catalog-list.html">Electricity</a>
                    </li>
                    <li>
                        <a href="catalog-gallery.html">Fasteners</a>
                    </li>
                    <li>
                        <a href="catalog-gallery-full.html">Lighting</a>
                    </li>
                    <li>
                        <a href="catalog-gallery.html">Pipes</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-2 align-center-mobile widget">
                <h3 class="widgettitle">Account</h3>
                <ul class="menu">
                    <li>
                        <a href="wishlist.html">Wishlist</a>
                    </li>
                    <li>
                        <a href="compare.html">Compare</a>
                    </li>
                    <li>
                        <a href="cart.html">Cart</a>
                    </li>
                    <li>
                        <a href="auth.html">My Account</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-2 align-center-mobile widget">
                <h3 class="widgettitle">Blog</h3>
                <ul class="menu">
                    <li>
                        <a href="blog.html">All Posts</a>
                    </li>
                    <li>
                        <a href="post.html">News</a>
                    </li>
                    <li>
                        <a href="post-slider.html">Articles</a>
                    </li>
                    <li>
                        <a href="blog.html">Reviews</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="form-validate modal-form" id="modal-form">
        <form action="#" method="POST" class="form-validate">
            <h4>Contact Us</h4>
            <input type="text" placeholder="Your name" data-required="text" name="name">
            <input type="text" placeholder="Your phone" data-required="text" name="phone">
            <input type="text" placeholder="Your email" data-required="text" data-required-email="email" name="email">
            <input class="btn1" type="submit" value="Send">
        </form>
    </div>

    <div class="cont maincont quick-view-modal">
        <article>
            <div class="prod">
                <div class="prod-slider-wrap prod-slider-shown">
                    <div class="flexslider prod-slider" id="prod-slider">
                        <ul class="slides">
                            <li>
                                <a data-fancybox-group="prod" class="fancy-img" href="http://placehold.it/1000x1000">
                                    <img src="http://placehold.it/550x550" alt="">
                                </a>
                            </li>
                            <li>
                                <a data-fancybox-group="prod" class="fancy-img" href="http://placehold.it/1000x1000">
                                    <img src="http://placehold.it/550x550" alt="">
                                </a>
                            </li>
                            <li>
                                <a data-fancybox-group="prod" class="fancy-img" href="http://placehold.it/1000x1000">
                                    <img  src="http://placehold.it/550x550" alt="">
                                </a>
                            </li>
                        </ul>
                        <div class="prod-slider-count"><p><span class="count-cur">1</span> / <span class="count-all">3</span></p><p class="hover-label prod-slider-zoom"><i class="icon ion-search"></i><span>Zoom In</span></p></div>
                    </div>
                    <div class="flexslider prod-thumbs" id="prod-thumbs">
                        <ul class="slides">
                            <li>
                                <img src="http://placehold.it/550x550" alt="">
                            </li>
                            <li>
                                <img src="http://placehold.it/550x550" alt="">
                            </li>
                            <li>
                                <img src="http://placehold.it/550x550" alt="">
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="prod-cont">
                    <div class="prod-rating-wrap">
                        <p data-rating="4" class="prod-rating">
                            <i class="rating-ico" title="1"></i><i class="rating-ico" title="2"></i><i class="rating-ico" title="3"></i><i class="rating-ico" title="4"></i><i class="rating-ico" title="5"></i>
                        </p>
                        <p class="prod-rating-count">7</p>
                    </div>
                    <p class="prod-categs"><a href="#">Lighting</a>, <a href="#">Tools</a></p>
                    <h2>Belt Sanders</h2>
                    <p class="prod-price">$25.00</p>
                    <p class="stock in-stock">7 in stock</p>
                    <p class="prod-excerpt">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget...</p>
                    <div class="prod-add">
                        <button type="submit"
                                class="button"><i class="icon ion-android-cart"></i> Add to cart
                        </button>
                        <p class="qnt-wrap prod-li-qnt">
                            <a href="#" class="qnt-plus prod-li-plus"><i class="icon ion-arrow-up-b"></i></a>
                            <input type="text" value="1">
                            <a href="#" class="qnt-minus prod-li-minus"><i class="icon ion-arrow-down-b"></i></a>
                        </p>
                        <div class="prod-li-favorites">
                            <a href="wishlist.html" class="hover-label add_to_wishlist"><i class="icon ion-heart"></i><span>Add to Wishlist</span></a>
                        </div>
                        <p class="prod-li-compare">
                            <a href="compare.html" class="hover-label prod-li-compare-btn"><span>Compare</span><i class="icon ion-arrow-swap"></i></a>
                        </p>
                    </div>
                </div>
            </div>
        </article>
    </div>
</div><!-- #page -->

<script src="/js/jquery-1.12.4.min.js"></script>
<script src="/js/jquery-plugins.js"></script>
<script src="/js/main.js"></script>

</body>
</html>